" Plugins
"" Plugin Manager
if has("win32")
    call plug#begin('$HOME\vimfiles\plugged')
else
    call plug#begin('~/.vim/plugged')
endif

""" Basics
Plug 'tpope/vim-sensible'
Plug 'sheerun/vim-polyglot'
Plug 'flazz/vim-colorschemes'
Plug 'jszakmeister/vim-togglecursor'
Plug 'airblade/vim-gitgutter'
Plug 'jiangmiao/auto-pairs'
Plug 'ervandew/supertab'
Plug 'tpope/vim-commentary'
Plug 'godlygeek/tabular'
Plug 'skywind3000/asyncrun.vim'
Plug 'tpope/vim-vinegar'
Plug 'embear/vim-localvimrc'
Plug 'airblade/vim-rooter'
Plug 'tpope/vim-dispatch'
Plug 'jeetsukumaran/vim-buffergator'
Plug 'vim-airline/vim-airline'
Plug 'mh21/errormarker.vim'
Plug 'ntpeters/vim-better-whitespace'
Plug 'nathanaelkane/vim-indent-guides'
Plug 'vitalk/vim-simple-todo'

"Plug 'tpope/vim-fugitive'
"Plug 'junegunn/gv.vim'
"Plug 'lifepillar/vim-mucomplete'
"Plug 'sirver/ultisnips'
"Plug 'honza/vim-snippets'
"Plug 'tpope/vim-projectionist'
"Plug 'amiorin/vim-project'
"Plug 'ctrlpvim/ctrlp.vim'
"Plug 'junegunn/goyo.vim'
"Plug 'junegunn/limelight.vim'
"Plug 'stefandtw/quickfix-reflector.vim'

call plug#end()
