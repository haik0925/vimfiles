set nocompatible

" General
let mapleader="\<Space>"
set hidden "Allow background buffers without saving
set spell spelllang=en_us
set splitright "Split to right by default

" Text Wrapping
set textwidth=80
set colorcolumn=81
set nowrap
" Search and Substitute
set gdefault "Use global flag by default in s: commands
set hlsearch "Highlight searches
set ignorecase
set smartcase "Don't ignore capitals in searches
nnoremap <leader>h :nohls <cr>

" Mapping - Switching to normal mode
inoremap jk <Esc>
inoremap jK <Esc>
inoremap Jk <Esc>
inoremap JK <Esc>
nnoremap <leader>w :w <cr>
nnoremap <leader>j :cn <cr>
nnoremap <leader>k :cp <cr>

" Tabs
set shiftwidth=4
set tabstop=4
set expandtab

" Backup, Swap, and Undo
set noundofile "Maintain undo history between sessions
set noswapfile
set nobackup
"if has("win32")
"    set directory=$HOME\vimfiles\swap,$TEMP
"    set backupdir=$HOME\vimfiles\backup,$TEMP
"    set undodir=$HOME\vimfiles\undo,$TEMP
"else
"    set directory=~/.vim/swap,/tmp
"    set backupdir=~/.vim/backup,/tmp
"    set undodir=~/.vim/undo,/tmp
"endif

"" Line number
set number

set si
set ai
