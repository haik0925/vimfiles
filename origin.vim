" Aliases
noremap <leader>src :source ~/_vimrc<cr>
nnoremap <leader>ve :Vexplore! <cr>
nnoremap <leader>e :Explore! <cr>
noremap <F7> :AsyncRun -program=make<cr>
nnoremap <F10> :call asyncrun#quickfix_toggle(6)<cr>

" Basic
set nospell

" Maxmize
au GUIEnter * simalt ~x

"" Python Version
augroup python3
    au! BufEnter *.py setlocal omnifunc=python3complete#Complete
augroup END

"" NetRW
"let g:netrw_liststyle=1 "Detail View
"let g:netrw_sizestyle="H" "Human-readable file sizes
"let g:netrw_list_hide='\(^\|\s\s\)\zs\.\S\+' "Hide dotfiles
"let g:netrw_hide=1 "Hide dotfiles by default
"let g:netrw_banner=0 "Turn off banner
""" Explore in vertical split

colorscheme onedark

" Build
" TODO: unix version of build.bat
set makeprg=build.bat



" Plugin Settings
"let g:auto_ctags=1
"let g:auto_ctags_directory_list = ['.git']
"let g:auto_ctags_filetype_mode = 1

let g:localvimrc_sandbox = 0
let g:localvimrc_ask = 0

let g:asyncrun_auto = 'make'
let g:asyncrun_open = 6
let g:asyncrun_status = ''
"let g:airline_section_error = airline#section#create_right(['%{g:asyncrun_status}'])

let g:buffergator_autoexpand_on_split = 0

let g:indent_guides_enable_on_vim_startup = 1
let g:indent_guides_start_level = 2
let g:indent_guides_guide_size = 1

let g:simple_todo_map_insert_mode_keys = 0
